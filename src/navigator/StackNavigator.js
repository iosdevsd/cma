import React, {useRef, useEffect} from 'react';
import {
  CardStyleInterpolators,
  createStackNavigator,
} from '@react-navigation/stack';
import {NavigationContainer} from '@react-navigation/native';
import Splash from '../Screens/Splash';
import SelectOption from '../Screens/SelectOption';
import SignUp from '../Screens/SignUp';
import Otp from '../Screens/Otp';
import Otp1 from '../Screens/Otp1';
import Login from '../Screens/Login';
import TabNavigator from './TabNavigator';
import EditProfile from '../Screens/EditProfile';
import UserList from '../Screens/UserList';
import Support from '../Screens/Support';
import Setting from '../Screens/Setting';
import Wallet from '../Screens/Wallet';
import Coupon from '../Screens/Coupon';
import TopTabNavigation from './TopTabNavigation';
import SageDetails from '../Screens/SageDetails';
import Decoration from '../Screens/Decoration';
import AddToCart from '../Screens/AddToCart';
import Productdetails from '../Screens/ProductDetails';
import ReviewYourOrder from '../Screens/ReviewYourOrder';
import Payment from '../Screens/Payment';
import SuccessOrder from '../Screens/SuccessOrder';
import OrderDetail from '../Screens/OrderDetail';
import BookingHistory from '../Screens/BookingHistory';
import ChatWithAstrologer from '../Screens/ChatWithAstrologer';
import AstrologerDetail from '../Screens/AstrologerDetail';
import RechargeWallet from '../Screens/RechargeWallet';
import SelectDateTime from '../Screens/SelectDateTime';
import HoroscopeDetail from '../Screens/HoroscopeDetail';
import OnlineAstrologer from '../Screens/OnlineAstrologer';
import Information from '../Screens/Information';
import Chat from '../Screens/Chat';
import LiveChat from '../Screens/LiveChat';
import LiveAudioCall from '../Screens/LiveAudioCall';
import LiveVideoCall from '../Screens/LiveVideoCall';

const Stack = createStackNavigator();
const StackNavigator = () => {
  const navigationRef = useRef();
  // useReduxDevToolsExtension(navigationRef);

  return (
    <NavigationContainer
      ref={navigationRef}
      // onReady={() => console.log(navigationRef.current.getCurrentRoute().name)}
      onStateChange={() =>
        console.log(
          `navigate :: ${navigationRef.current.getCurrentRoute().name}`,
        )
      }>
      <Stack.Navigator
        initialRouteName={'Splash'}
        screenOptions={{
          cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        }}>
        <Stack.Screen
          name="Splash"
          component={Splash}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="SelectOption"
          component={SelectOption}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="SignUp"
          component={SignUp}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Otp"
          component={Otp}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Login"
          component={Login}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="TabNavigator"
          component={TabNavigator}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="EditProfile"
          component={EditProfile}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="UserList"
          component={UserList}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Support"
          component={Support}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Setting"
          component={Setting}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Otp1"
          component={Otp1}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Wallet"
          component={Wallet}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Coupon"
          component={Coupon}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="TopTabNavigation"
          component={TopTabNavigation}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="SageDetails"
          component={SageDetails}
          options={{headerShown: false}}
        />

        <Stack.Screen
          name="Decoration"
          component={Decoration}
          options={{headerShown: false}}
        />

        <Stack.Screen
          name="AddToCart"
          component={AddToCart}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Productdetails"
          component={Productdetails}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="ReviewYourOrder"
          component={ReviewYourOrder}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Payment"
          component={Payment}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="SuccessOrder"
          component={SuccessOrder}
          options={{headerShown: false}}
        />

        <Stack.Screen
          name="OrderDetail"
          component={OrderDetail}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="BookingHistory"
          component={BookingHistory}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="ChatWithAstrologer"
          component={ChatWithAstrologer}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="AstrologerDetail"
          component={AstrologerDetail}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="RechargeWallet"
          component={RechargeWallet}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="SelectDateTime"
          component={SelectDateTime}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="HoroscopeDetail"
          component={HoroscopeDetail}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="OnlineAstrologer"
          component={OnlineAstrologer}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Information"
          component={Information}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Chat"
          component={Chat}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="LiveChat"
          component={LiveChat}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="LiveVideoCall"
          component={LiveVideoCall}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="LiveAudioCall"
          component={LiveAudioCall}
          options={{headerShown: false}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default StackNavigator;
