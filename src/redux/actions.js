export const SetDeviceInfo = deviceInfo => ({
  type: 'setDeviceInfo',
  payload: deviceInfo,
});
export const SetNetInfo = payload => ({type: 'setNetInfo', payload});
export const SetHome = payload => ({type: 'setHome', payload});
export const SetSpecialities = payload => ({type: 'setSpecialities', payload});
export const SetUserDetail = payload => ({type: 'setUserDetail', payload});
export const SetLogout = () => ({type: 'setLogout'});
export const SetWallet = payload => ({type: 'setWallet', payload});
export const SetInformation = payload => ({type: 'setInformation', payload});
export const SetFaq = payload => ({type: 'setFaq', payload});
export const SetMembersList = payload => ({type: 'setMembersList', payload});
export const SetAstroShop = payload => ({type: 'setAstroShop', payload});
export const Addresses = payload => ({type: 'addresses', payload});

export const CartUpdate = payload => ({type: 'cartUpdate', payload});
export const OnlineAstrologer = payload => ({
  type: 'onlineAstrologer',
  payload,
});
