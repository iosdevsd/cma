import React, {useState} from 'react';
import {
  FlatList,
  Image,
  ImageBackground,
  ScrollView,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import DashedLine from 'react-native-dashed-line';
import {StatusBarLight} from '../Custom/CustomStatusBar';

import {
  BottomButton,
  CustomInputField,
  Header,
  MainView,
} from '../Custom/CustomView';

const OrderDetail = ({navigation}) => {
  const [person, setPerson] = useState([
    {
      key: 1,
      order: 'Order #123456',
      date: '06/03/2021 - 10:30 AM',
      status: 'Status: ',
      ongoing: 'Completed',
      source: require('../assets/produt.png'),
      stonetype: '6 Ratti Blue Sapphire (Neelam) Gen..',
      quantity: 'Quantity : 1',
      mrp: 'MPR:',
      amount: '1800/-',
      total: 'Total: ₹3600/- ',
    },
  ]);
  return (
    <MainView>
      <StatusBarLight />

      <Header
        onPress={() => navigation.navigate('CompleteOrder')}
        title={'Order Detail'}
      />
      <ScrollView>
        <View style={{width: '100%', marginTop: 5}}>
          <FlatList
            data={person}
            numColumns={1}
            renderItem={({item}) => {
              return (
                <>
                  <ImageBackground
                    source={require('../assets/order-bg.png')}
                    style={styles.bg}>
                    <View style={styles.ftview}>
                      <Text style={styles.ftText}>{item.order}</Text>
                      <Text style={styles.ftText1}>{item.date}</Text>
                    </View>

                    <View style={{padding: '5%', marginHorizontal: 10}}>
                      <DashedLine dashColor={'#6F6F7B'} dashLength={5} />
                    </View>

                    <Text style={styles.ftText2}>
                      {item.status}
                      <Text style={[styles.ftText2, {color: '#00C327'}]}>
                        {item.ongoing}
                      </Text>
                    </Text>

                    <View style={styles.ftview_1}>
                      <Image source={item.source} style={styles.ftImg} />
                      <View style={styles.ftview_2}>
                        <Text style={styles.ftText_2}>{item.stonetype}</Text>
                        <Text style={styles.ftText_1}>{item.quantity}</Text>
                        <Text style={styles.ftText_1}>
                          {item.mrp}
                          <Text style={[styles.ftText_1, {color: '#6266F9'}]}>
                            {item.amount}
                          </Text>
                        </Text>
                      </View>
                    </View>

                    <View style={{padding: '5%', marginHorizontal: 10}}>
                      <DashedLine dashColor={'#6F6F7B'} dashLength={5} />
                    </View>

                    <View style={styles.ftview_1}>
                      <Image source={item.source} style={styles.ftImg} />
                      <View style={styles.ftview_2}>
                        <Text style={styles.ftText_2}>{item.stonetype}</Text>
                        <Text style={styles.ftText_1}>{item.quantity}</Text>
                        <Text style={styles.ftText_1}>
                          {item.mrp}
                          <Text style={[styles.ftText_1, {color: '#6266F9'}]}>
                            {item.amount}
                          </Text>
                        </Text>
                      </View>
                    </View>

                    <View style={{padding: '5%', marginHorizontal: 10}}>
                      <DashedLine dashColor={'#6F6F7B'} dashLength={5} />
                    </View>

                    <Text style={styles.ftText_3}>
                      Total:
                      <Text style={[styles.total, {color: '#6266F9'}]}>
                        ₹3600/-
                      </Text>
                    </Text>
                  </ImageBackground>
                </>
              );
            }}
          />
        </View>

        <View
          style={{
            width: '90%',
            alignSelf: 'center',
            borderColor: '#FFFFFF40',
            marginTop: 20,
            borderRadius: 10,
            borderWidth: 0.8,
          }}>
          <View
            style={{
              width: '90%',
              alignSelf: 'center',
              marginTop: 10,
              alignItems: 'center',
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>
            <Text
              style={{
                fontSize: 14,
                lineHeight: 18,
                fontFamily: 'Avenir-Regular',
                color: '#FFF',
                fontWeight: '400',
              }}>
              Sub Total
            </Text>
            <Text
              style={{
                fontSize: 14,
                lineHeight: 18,
                fontFamily: 'Avenir-Regular',
                color: '#FFF',
                fontWeight: '400',
              }}>
              ₹3600.00
            </Text>
          </View>

          <View
            style={{
              width: '90%',
              alignSelf: 'center',
              marginTop: 7,
              alignItems: 'center',
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>
            <Text
              style={{
                fontSize: 14,
                lineHeight: 18,
                fontFamily: 'Avenir-Regular',
                color: '#FFF',
                fontWeight: '400',
              }}>
              Delivery Charges
            </Text>
            <Text
              style={{
                fontSize: 14,
                lineHeight: 18,
                fontFamily: 'Avenir-Regular',
                color: '#FFF',
                fontWeight: '400',
              }}>
              ₹20.00
            </Text>
          </View>

          <View
            style={{
              width: '90%',
              alignSelf: 'center',
              marginTop: 7,
              alignItems: 'center',
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>
            <Text
              style={{
                fontSize: 14,
                lineHeight: 18,
                fontFamily: 'Avenir-Regular',
                color: '#FFF',
                fontWeight: '400',
              }}>
              Total Payable
            </Text>
            <Text
              style={{
                fontSize: 14,
                lineHeight: 18,
                fontFamily: 'Avenir-Regular',
                color: '#FFF',
                fontWeight: '400',
              }}>
              ₹3620.00
            </Text>
          </View>

          <View
            style={{
              width: '100%',
              borderBottomLeftRadius: 10,
              borderBottomRightRadius: 10,
              marginTop: 7,
              backgroundColor: '#4E4FC115',
              justifyContent: 'center',
              height: 51,
            }}>
            <View
              style={{
                width: '90%',
                alignSelf: 'center',
                alignItems: 'center',
                flexDirection: 'row',
                justifyContent: 'space-between',
              }}>
              <Text
                style={{
                  fontSize: 12,
                  lineHeight: 18,
                  fontFamily: 'Avenir-Bold',
                  color: '#6266F9',
                  fontWeight: '700',
                }}>
                TOTAL AMOUNT PAY
              </Text>
              <Text
                style={{
                  fontSize: 12,
                  lineHeight: 18,
                  fontFamily: 'Avenir-Bold',
                  color: '#6266F9',
                  fontWeight: '700',
                }}>
                ₹3620.00
              </Text>
            </View>
          </View>
        </View>

        <View
          style={{
            width: '90%',
            alignSelf: 'center',
            borderColor: '#FFFFFF40',
            marginTop: 20,
            borderRadius: 10,
            borderWidth: 0.8,
            marginBottom: 30,
          }}>
          <View style={{width: '90%', alignSelf: 'center'}}>
            <Text
              style={{
                fontSize: 14,
                lineHeight: 18,
                fontFamily: 'Avenir-Regular',
                color: '#FFF',
                fontWeight: '500',
                marginTop: 14,
              }}>
              Shipping Address:
            </Text>

            <Text
              style={{
                marginBottom: 20,
                marginTop: 8,
                fontSize: 12,
                lineHeight: 20,
                fontFamily: 'Avenir-Regular',
                color: '#FFFFFF80',
                fontWeight: '600',
              }}>
              C9/21, 2nd floor, Rithala Road Opp. Metro Pillar No.400, Sector-7,
              Rohini, New Delhi, Delhi 110085,New Delhi, Delhi - 110085,Mobile
              No: +91-9599499793
            </Text>
          </View>
        </View>
      </ScrollView>
    </MainView>
  );
};

export default OrderDetail;

const styles = StyleSheet.create({
  bg: {
    width: 335,
    height: 370,
    resizeMode: 'contain',
    // marginLeft: 15,
    marginTop: 10,
    alignSelf: 'center',
  },
  ftview: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 10,
    marginHorizontal: 30,
    // marginLeft: 10,
    // alignSelf: 'center',
  },
  ftText: {
    fontFamily: 'Avenir-Medium',
    fontSize: 16,
    fontWeight: '600',
    color: '#FFFFFF',
  },
  ftText1: {
    fontFamily: 'Avenir-Medium',
    fontSize: 12,
    fontWeight: '600',
    color: '#FFFFFF',
    opacity: 0.5,
  },
  dash: {
    borderColor: '#6F6F7B',
    marginHorizontal: 20,
    borderStyle: 'dotted',
    borderWidth: 1,
    marginTop: 10,
  },
  ftText2: {
    fontFamily: 'Avenir-Medium',
    fontSize: 14,
    fontWeight: '600',
    color: '#FFFFFF',
    opacity: 0.8,
    marginHorizontal: 30,
  },
  ftImg: {
    width: 80,
    height: 80,
    resizeMode: 'contain',
  },
  ftview_1: {
    flexDirection: 'row',
    marginHorizontal: 30,
    // backgroundColor: 'yellow',
    marginTop: 10,
  },
  ftview_2: {
    marginHorizontal: 10,
    // backgroundColor: 'yellow',
    width: '70%',
  },
  ftText_1: {
    fontFamily: 'Avenir-Medium',
    fontSize: 15,
    fontWeight: '600',
    color: '#FFFFFF',
    opacity: 0.8,
    marginTop: 3,
  },
  ftText_2: {
    fontFamily: 'Avenir-Medium',
    fontSize: 16,
    fontWeight: '600',
    color: '#FFFFFF',
  },
  ftText_3: {
    fontFamily: 'Avenir-Medium',
    fontSize: 16,
    fontWeight: '600',
    color: '#FFFFFF',
    marginHorizontal: 30,
  },
});
