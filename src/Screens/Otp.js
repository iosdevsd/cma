import React, {useState} from 'react';
import {
  Text,
  StyleSheet,
  Image,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import {StatusBarLight} from '../Custom/CustomStatusBar';
import {MainView} from '../Custom/CustomView';
import OTPInputView from '@twotalltotems/react-native-otp-input';
import {useStore} from 'react-redux';
import {useDispatch} from 'react-redux';
import * as actions from '../redux/actions';
import {Api, LocalStorage} from '../services/Api';
import {_SetAuthToken} from '../services/ApiSauce';
import Loader from '../component/Loader';

const Otp = ({navigation, route}) => {
  const [state, setState] = useState({
    otp: '',
    isLoading: false,
  });
  const {deviceInfo} = useStore().getState();
  const dispatch = useDispatch();

  const toggleLoader = isLoading => setState({...state, isLoading});

  const onResendHandler = async () => {
    const {id} = route.params.data;
    toggleLoader(true);
    const response = await Api.resendOtp({id});
    toggleLoader(false);
    const {status} = response;
    if (status) {
      alert('New OTP send successfully');
    } else {
      alert('Something went wrong please try later');
    }
  };

  const onVerifyHandler = async otp => {
    const {id, type} = route.params.data;
    if (otp.length !== 4) {
      alert('Please enter your valid OTP');
      return;
    }
    toggleLoader(true);
    const response = await Api.otpVerify({
      id,
      otp,
      device_id: deviceInfo.id,
      device_type: deviceInfo.os,
      device_token: deviceInfo.token,
      model_name: deviceInfo.model,
    });
    var d = {
      id,
      otp,
      device_id: deviceInfo.id,
      device_type: deviceInfo.os,
      device_token: deviceInfo.token,
      model_name: deviceInfo.model,
    };
    consolejson(d);
    toggleLoader(false);
    const {
      status = false,
      data = {},
      token = '',
      message = 'Something went wrong',
    } = response;
    consolejson(response);
    if (status) {
      LocalStorage.setToken(token);
      _SetAuthToken(token);

        dispatch(actions.SetUserDetail(data));
        navigation.reset({
          index: 0,
          routes: [{name: 'TabNavigator'}],
        });


    } else {
      setState({...state, otp: ''});
      alert(message);
    }
  };

  return (
    <MainView>
      <StatusBarLight />
      <Loader status={state.isLoading} />
      <ScrollView>
        <Text style={styles.textStyles1}>OTP</Text>
        <Text style={styles.OtpText}>Phone Verification !</Text>

        <Text style={styles.textStyles2}>
          {`Please enter code that we’ve sent to your mobile number `}
        </Text>
        <Image
          style={styles.imageStyles}
          source={require('../assets/otp.png')}
        />

        <OTPInputView
          style={styles.otpInput}
          pinCount={4}
          code={state.otp}
          onCodeChanged={otp => setState({...state, otp})}
          autoFocusOnLoad
          codeInputFieldStyle={styles.underlineStyleBase}
          codeInputHighlightStyle={styles.underlineStyleHighLighted}
          onCodeFilled={otp => onVerifyHandler(otp)}
        />
        <Text style={styles.textStyles4}>Didn’t you received any code?</Text>
        <TouchableOpacity onPress={onResendHandler}>
          <Text style={styles.textStyles5}>RESEND CODE</Text>
        </TouchableOpacity>
      </ScrollView>
    </MainView>
  );
};

export default Otp;

const styles = StyleSheet.create({
  imageStyles: {
    height: 240,
    width: 194,
    resizeMode: 'contain',
    alignSelf: 'center',
  },
  textStyles1: {
    fontFamily: 'Avenir-Medium',
    fontSize: 72,
    fontWeight: 'bold',
    color: '#6F6F7B',
    marginTop: 30,
    opacity: 0.3,
  },
  OtpText: {
    fontFamily: 'Avenir-Medium',
    fontSize: 24,
    fontWeight: '600',
    color: '#FFFFFF',
    marginHorizontal: 30,
    bottom: 40,
  },
  textStyles2: {
    fontFamily: 'Avenir',
    fontSize: 16,
    fontWeight: '500',
    color: '#FFFFFF',
    marginHorizontal: 30,
    opacity: 0.5,
  },

  otpInput: {
    width: '84%',
    height: 60,
    alignSelf: 'center',
  },
  underlineStyleBase: {
    width: 60,
    height: 60,
    borderWidth: 0.5,
    backgroundColor: '#1B172C',
    borderRadius: 30,
    fontSize: 35,
    color: '#ffffff',
    borderColor: 'white',
  },
  underlineStyleHighLighted: {
    width: 60,
    height: 60,
    backgroundColor: '#ffffff80',
    fontSize: 35,
    color: '#1B172C',
    borderColor: '#1B172C',
  },
  textStyles4: {
    fontFamily: 'Avenir',
    fontSize: 14,
    fontWeight: '500',
    color: '#ffffff',
    opacity: 0.5,
    alignSelf: 'center',
    marginTop: 20,
  },
  textStyles5: {
    fontFamily: 'Avenir',
    fontSize: 16,
    fontWeight: 'bold',
    color: '#6266F9',
    alignSelf: 'center',
    marginBottom: 30,
    marginTop: 5,
  },
});
