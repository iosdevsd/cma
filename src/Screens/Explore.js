import React from 'react';
import {StatusBar, ScrollView, TouchableOpacity, Text} from 'react-native';
import {StatusBarLight} from '../Custom/CustomStatusBar';
import {MainView} from '../Custom/CustomView';
import Header from './Explore/Header';
import ConnectLive from './Explore/ConntectLive';
import Services from './Explore/Services';
import Deal from './Explore/Deal';
import Puja from './Explore/Puja';
import Horoscope from './Explore/Horoscope';
import Banner from './Explore/Banner';
import Saga from './Explore/Saga';
import Footer from './Explore/Footer';
import RequestHandler from './requestHandler/RequestHandler';
import {useSelector} from 'react-redux';

const Explore = () => {
  const {isLogin} = useSelector(store => store);
  const onPresshandler = () => {
    const body = {
      astrologer_id: 10,
      price_per_mint: 100,
      type: '1',
      user_id: 145,
      member_id: 1042,
    };
    consolejson(body);
  };

  return (
    <MainView>
      <StatusBarLight />
      <ScrollView contentContainerStyle={{paddingTop: StatusBar.currentHeight}}>
        <Header />
        <Banner />

        <Services />
        <Horoscope />
        {/* <Deal /> */}
        {/* <Puja /> */}
        {/* <Saga /> */}
        <Footer />
      </ScrollView>
      {isLogin && <RequestHandler />}
      <TouchableOpacity onPress={onPresshandler}>
        <Text>call</Text>
      </TouchableOpacity>
    </MainView>
  );
};

export default Explore;
