import React, {useState} from 'react';
import {
  FlatList,
  Image,
  Modal,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {StatusBarLight} from '../Custom/CustomStatusBar';
import {MainView} from '../Custom/CustomView';

const AstrologerDetail = ({navigation}) => {
  const Box = props => (
    <TouchableOpacity
      onPress={props.onPress}
      style={[styles.box, {backgroundColor: props.bgcolor}]}>
      <View style={styles.toprow}>
        <Image source={props.boxsource} style={styles.bottomImg} />
        <Text style={styles.boxText}>{props.title}</Text>
      </View>
    </TouchableOpacity>
  );

  const [modalOpen, setModalOpen] = useState(false);

  const [person, setPerson] = useState([
    {
      key: 1,
      detail:
        'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
      source: require('../assets/r.png'),
      name: 'Rahul Sharma',
      date: '15/03/2020',
    },
    {
      key: 2,
      detail:
        'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
      source: require('../assets/r.png'),
      name: 'Rahul Sharma',
      date: '15/03/2020',
    },
    {
      key: 3,
      detail:
        'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
      source: require('../assets/r.png'),
      name: 'Rahul Sharma',
      date: '15/03/2020',
    },
    {
      key: 4,
      detail:
        'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
      source: require('../assets/r.png'),
      name: 'Rahul Sharma',
      date: '15/03/2020',
    },
    {
      key: 5,
      detail: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.`,
      source: require('../assets/r.png'),
      name: 'Rahul Sharma',
      date: '15/03/2020',
    },
    {
      key: 6,
      detail:
        'Lorem Ipsum is simply dummy text of \n the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
      source: require('../assets/r.png'),
      name: 'Rahul Sharma',
      date: '15/03/2020',
    },
  ]);

  return (
    <MainView>
      <StatusBarLight />
      <ScrollView>
        <View style={styles.topview}>
          <View style={styles.topview1}>
            <TouchableOpacity onPress={navigation.goBack}>
              <Image
                style={styles.arrow}
                source={require('../assets/back.png')}
              />
            </TouchableOpacity>
            <Text style={styles.toptext}>Vipul Pandey</Text>
          </View>

          <View style={styles.headerView}>
            <Image
              style={styles.coinimage}
              source={require('../assets/coin.png')}
            />
            <Text style={styles.headertext}>0</Text>
          </View>

          <Image
            style={styles.filter}
            source={require('../assets/share1.png')}
          />
        </View>

        <Image
          style={styles.middle}
          source={require('../assets/astrologer1.png')}
        />

        <View style={styles.submiddle}>
          <Box
            onPress={() => setModalOpen(true)}
            bgcolor="#E91E63"
            boxsource={require('../assets/chatting.png')}
            title={'₹100/min'}
          />

          <Box
            bgcolor="#9C27B0"
            boxsource={require('../assets/telephone.png')}
            title={'₹100/min'}
          />

          <Box
            onPress={() => navigation.navigate('SelectDateTime')}
            bgcolor="#673AB7"
            boxsource={require('../assets/video-camera2.png')}
            title={'₹100/min'}
          />
        </View>

        <Modal visible={modalOpen} transparent={true}>
          <View style={styles.madal_View}>
            <View style={styles.madal_Style}>
              <View style={styles.mdv3_Style}>
                <Text style={styles.mdt_style}>
                  {`Insufficient wallet \nbalance !`}
                </Text>

                <View style={styles.mdv_Style}>
                  <Text
                    style={{
                      fontFamily: 'Avenir',
                      fontSize: 17,
                      fontWeight: '400',
                      color: '#FFFFFF',
                    }}>
                    Available Balance:
                  </Text>

                  <View style={styles.mdv1_Style}>
                    <Text style={styles.mdt1_Style}>₹ 0.00</Text>
                  </View>
                </View>

                <View style={styles.mdv2_Style} />
                <Text style={styles.mdt2_Style}>
                  {`Minimum wallet balance required to \ntalk with Vipul Pandey is ₹ 200.00.\nPlease recharge your wallet.`}
                </Text>
              </View>

              <View style={styles.mdv5_Style}>
                <View
                  onPress={() => setModalOpen(false)}
                  style={styles.mdv4_Style}>
                  <TouchableOpacity onPress={() => setModalOpen(false)}>
                    <Text style={styles.bt_style}>CANCEL</Text>
                  </TouchableOpacity>

                  <TouchableOpacity
                    onPress={() => {
                      setModalOpen(false);
                      navigation.navigate('RechargeWallet');
                    }}>
                    <Text style={styles.bt_style}>RECHARGE</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </View>
        </Modal>

        <Text style={[styles.toptext, {fontSize: 25, marginTop: 10}]}>
          Vipul Pandey
        </Text>
        <Text style={[styles.toptext, {fontSize: 15, opacity: 0.7}]}>
          Vedic Astrology
        </Text>
        <View style={styles.rating}>
          <Image
            style={styles.starimage}
            source={require('../assets/star-astro.png')}
          />
          <Image
            style={styles.starimage}
            source={require('../assets/star-astro.png')}
          />
          <Image
            style={styles.starimage}
            source={require('../assets/star-astro.png')}
          />
          <Image
            style={styles.starimage}
            source={require('../assets/star-astro.png')}
          />
        </View>
        <View style={styles.middleView}>
          <View style={{marginRight: 50}}>
            <Text style={styles.middletext}>Chat</Text>
            <Text style={styles.middletext1}>9K mins</Text>
          </View>

          <View style={{marginLeft: 0}}>
            <Text style={styles.middletext}>Chat</Text>
            <Text style={styles.middletext1}>9K mins</Text>
          </View>

          <View style={{marginLeft: 50}}>
            <Text style={styles.middletext}>Chat</Text>
            <Text style={styles.middletext1}>9K mins</Text>
          </View>
        </View>

        <View style={styles.line} />

        <Text style={styles.abouttext}>About</Text>
        <Text style={[styles.middletext, {marginHorizontal: 20}]}>
          Lorem Ipsum is simply dummy text of the printing and typesetting
          industry. Lorem Ipsum has been the industry's standard dummy text ever
          since the 1500s.
        </Text>
        <Text style={[styles.abouttext, {color: '#3F55F6', fontSize: 13}]}>
          Read more…
        </Text>

        <View style={styles.middleView}>
          <View style={{marginRight: 40}}>
            <Text style={styles.middletext}>Experience</Text>
            <Text style={styles.middletext1}>8 Years</Text>
          </View>

          <View style={{marginLeft: 0}}>
            <Text style={styles.middletext}>Language</Text>
            <Text style={styles.middletext1}>English, Hindi</Text>
          </View>

          <View style={{marginLeft: 25}}>
            <Text style={styles.middletext}>Price</Text>
            <Text style={styles.middletext1}>300/min</Text>
          </View>
        </View>

        <Text style={styles.middletext2}>Video Message</Text>

        <Image style={styles.video} source={require('../assets/video1.png')} />

        <Text style={styles.middletext2}>Rating & Reviews</Text>

        <View style={styles.rating}>
          <Text style={styles.middletext3}>
            4.0
            <Text style={[styles.middletext3, {fontSize: 18}]}>/5</Text>
          </Text>

          <View style={styles.rating}>
            <Image
              style={styles.starimage1}
              source={require('../assets/star-astro.png')}
            />
            <Image
              style={styles.starimage1}
              source={require('../assets/star-astro.png')}
            />
            <Image
              style={styles.starimage1}
              source={require('../assets/star-astro.png')}
            />
            <Image
              style={styles.starimage1}
              source={require('../assets/star-astro.png')}
            />
          </View>
        </View>

        <Text style={styles.btText}>32 reviews</Text>

        <FlatList
          data={person}
          //   numColumns={2}
          horizontal={true}
          renderItem={({item}) => {
            return (
              <View style={styles.ftView}>
                <Text style={styles.fttoptext}>{item.detail}</Text>
                <View style={styles.ftbottomview}>
                  <Image style={styles.ftimage} source={item.source} />
                  <View style={{marginHorizontal: 10, marginTop: 5}}>
                    <Text style={styles.middletext1}>{item.name}</Text>
                    <Text style={styles.ftbtText}>{item.date}</Text>
                  </View>
                </View>
              </View>
            );
          }}
        />
      </ScrollView>
    </MainView>
  );
};

export default AstrologerDetail;

const styles = StyleSheet.create({
  arrow: {
    width: 12,
    height: 20,
    resizeMode: 'contain',
  },
  topview: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: StatusBar.currentHeight + 10,
    marginHorizontal: 20,
    alignItems: 'center',
  },
  toptext: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    color: '#FFFFFF',
    marginHorizontal: 10,
    marginLeft: 20,
  },
  filter: {
    width: 20,
    height: 18,
    resizeMode: 'contain',
  },
  topview1: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  headerView: {
    height: 30,
    width: 60,
    borderRadius: 25,
    justifyContent: 'center',
    borderWidth: 1,
    marginLeft: 40,
    borderColor: '#FFFFFFaa',
    flexDirection: 'row',
    alignItems: 'center',
  },
  headerimage: {
    height: 20,
    width: 20,
    resizeMode: 'contain',
    // alignSelf: 'center',
  },
  headertext: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 22,
    fontWeight: 'bold',
    color: '#FFFFFF',
    marginHorizontal: 5,
  },
  coinimage: {
    width: 25,
    height: 25,
    resizeMode: 'contain',
  },
  middle: {
    width: 200,
    height: 268,
    resizeMode: 'contain',
    alignSelf: 'center',
    marginTop: 20,
  },
  box: {
    // backgroundColor: '#E91E63',
    borderRadius: 5,
    padding: 7,
    width: '30%',
    marginTop: 20,
    // marginHorizontal: 50,
  },
  toprow: {
    flexDirection: 'row',
    alignItems: 'center',
    // marginHorizontal: 10,
    alignSelf: 'center',
  },
  bottomImg: {
    width: 18,
    height: 16,
    resizeMode: 'contain',
  },
  boxText: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 13,
    fontWeight: 'bold',
    color: '#FFFFFF',
    marginHorizontal: 5,
  },
  submiddle: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginHorizontal: 10,
  },
  rating: {
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 20,
    marginTop: 0,
  },
  starimage: {
    width: 14,
    height: 13,
    resizeMode: 'contain',
    marginTop: 5,
  },
  middleView: {
    flexDirection: 'row',
    alignItems: 'center',
    // marginHorizontal: 20,
    marginTop: 10,
    // marginRight: 50,
    justifyContent: 'space-evenly',
  },
  middletext: {
    fontFamily: 'Avenir-Medium',
    fontSize: 13,
    fontWeight: '400',
    color: '#677294',
    // marginHorizontal: 5,
  },
  middletext1: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 15,
    fontWeight: '900',
    color: '#FFFFFF',
    // marginHorizontal: 5,
  },
  line: {
    borderColor: '#EDEEEF',
    borderWidth: 0.5,
    marginHorizontal: 20,
    opacity: 0.5,
    marginTop: 10,
  },
  abouttext: {
    fontFamily: 'Avenir-Medium',
    fontSize: 18,
    fontWeight: '500',
    color: '#FFFFFF',
    marginHorizontal: 20,
    marginTop: 10,
  },
  middletext2: {
    fontFamily: 'Avenir-Medium',
    fontSize: 18,
    fontWeight: '500',
    color: '#FFFFFF',
    marginHorizontal: 20,
    marginTop: 10,
  },
  video: {
    width: 328,
    height: 125,
    resizeMode: 'contain',
    marginTop: 20,
    marginBottom: 20,
    marginHorizontal: 20,
  },
  starimage1: {
    width: 22,
    height: 22,
    resizeMode: 'contain',
    marginTop: 5,
    marginHorizontal: 5,
  },
  middletext3: {
    fontFamily: 'Avenir-Medium',
    fontSize: 30,
    fontWeight: '500',
    marginTop: 10,
    color: '#FFC901',
  },
  btText: {
    fontFamily: 'Avenir-Medium',
    fontSize: 13,
    fontWeight: '600',
    marginTop: 10,
    color: '#FFFFFF',
    marginHorizontal: 20,
  },
  ftView: {
    marginHorizontal: 20,
    width: 260,
    marginTop: 10,
    // marginLeft: 7,
    padding: 10,
    borderRadius: 15,
    backgroundColor: '#13042A',
    // backgroundColor: 'yellow',
    marginBottom: 20,
  },
  ftText: {
    fontFamily: 'Avenir-Medium',
    fontSize: 13,
    fontWeight: '500',
    // marginTop: 10,
    color: '#FFFFFF',
    marginHorizontal: 20,
  },
  ftbottomview: {
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 10,
  },
  ftimage: {
    width: 35,
    height: 35,
    resizeMode: 'contain',
    // marginHorizontal: 5,
    marginTop: 10,
  },
  ftbtText: {
    fontFamily: 'Avenir-Medium',
    fontSize: 11,
    fontWeight: '500',
    color: '#677294',
    // marginHorizontal: 5,
  },
  fttoptext: {
    fontFamily: 'Avenir-Medium',
    fontSize: 13,
    fontWeight: '500',
    color: '#FFFFFF',
    marginHorizontal: 5,
  },
  madal_View: {
    flex: 1,
    backgroundColor: '#000000aa',
  },
  madal_Style: {
    backgroundColor: '#0E0220',
    width: '90%',
    // marginHorizontal: 0,
    alignSelf: 'center',
    borderRadius: 20,
    marginTop: 200,
    // paddingBottom: 10,
  },
  mdt_style: {
    fontFamily: 'Avenir',
    fontSize: 22,
    fontWeight: 'bold',
    color: '#FFFFFF',
  },
  mdt1_Style: {
    fontFamily: 'Avenir',
    fontSize: 17,
    fontWeight: '400',
    color: '#1D1E2C',
  },
  mdv_Style: {
    marginTop: 10,
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-between',
  },
  mdv1_Style: {
    width: '25%',
    backgroundColor: '#3F55F6',
    alignItems: 'center',
    borderRadius: 20,
    padding: 7,
    marginTop: -5,
  },
  mdt1_Style: {
    fontFamily: 'Avenir',
    fontSize: 14,
    fontWeight: '500',
    color: '#FFFFFF',
  },
  mdv2_Style: {
    borderColor: '#EDEEEF',
    opacity: 0.1,
    borderWidth: 0.5,
    marginTop: 15,
  },
  mdt2_Style: {
    fontFamily: 'Avenir',
    fontSize: 16,
    fontWeight: '400',
    color: '#FFFFFF',
    marginTop: 10,
  },
  mdv3_Style: {
    padding: 20,
  },
  mdv4_Style: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  mdv5_Style: {
    width: '100%',
    backgroundColor: '#3F55F6',
    marginTop: 20,
    padding: 10,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
    //alignItems: 'center',
  },
  bt_style: {
    fontFamily: 'Avenir',
    fontSize: 18,
    fontWeight: 'bold',
    color: '#FFFFFF',
  },
});
