import React from 'react';
import {
  View,
  Text,
  StatusBar,
  StyleSheet,
  ImageBackground,
  TouchableOpacity,
  Image,
  ScrollView,
} from 'react-native';
import {StatusBarLight} from '../Custom/CustomStatusBar';
import {MainView} from '../Custom/CustomView';

const Sage = ({navigation}) => {
  const TouchBox = props => (
    <TouchableOpacity activeOpacity={0.7} onPress={props.onPress}>
      <ImageBackground source={props.bgsource} style={styles.bg}>
        <Text style={styles.touchboxText}>{props.title}</Text>

        <View style={styles.touchbox}>
          <Image source={props.dotsource} style={styles.dotsource} />
          <Text style={styles.touchboxText1}>{props.subtitle}</Text>
        </View>
      </ImageBackground>
    </TouchableOpacity>
  );

  return (
    <MainView>
      <StatusBarLight />
      <ScrollView>
        <Text style={styles.toptext}>Insight Timer</Text>
        <Text style={styles.toptext1}>What brings you here?</Text>

        <View style={styles.middleView}>
          <TouchBox
            bgsource={require('../assets/1.png')}
            title="Mantras"
            dotsource={require('../assets/dot.png')}
            subtitle="EXPLORE"
            onPress={() => navigation.navigate('SageDetails')}
          />
          <TouchBox
            bgsource={require('../assets/2.png')}
            title="Earthling"
            dotsource={require('../assets/dot.png')}
            subtitle="EXPLORE"
            onPress={() => navigation.navigate('SageDetails')}
          />
          <TouchBox
            bgsource={require('../assets/3.png')}
            title="Moon & Astrology"
            dotsource={require('../assets/dot.png')}
            subtitle="EXPLORE"
            onPress={() => navigation.navigate('SageDetails')}
          />
          <TouchBox
            bgsource={require('../assets/4.png')}
            title="Chromotherapy"
            dotsource={require('../assets/dot.png')}
            subtitle="EXPLORE"
            onPress={() => navigation.navigate('SageDetails')}
          />
          <TouchBox
            bgsource={require('../assets/5.png')}
            title="Eclipses"
            dotsource={require('../assets/dot.png')}
            subtitle="EXPLORE"
            onPress={() => navigation.navigate('SageDetails')}
          />
          <TouchBox
            bgsource={require('../assets/6.png')}
            title="Moon Calendar"
            dotsource={require('../assets/dot.png')}
            subtitle="EXPLORE"
            onPress={() => navigation.navigate('SageDetails')}
          />
        </View>
      </ScrollView>
    </MainView>
  );
};

export default Sage;

const styles = StyleSheet.create({
  toptext: {
    fontFamily: 'Avenir-Medium',
    fontSize: 18,
    fontWeight: '500',
    color: '#FFFFFF',
    marginTop: 50,
    alignSelf: 'center',
  },
  toptext1: {
    fontFamily: 'Avenir-Medium',
    fontSize: 21,
    fontWeight: 'bold',
    color: '#FFFFFF',
    marginTop: 10,
    alignSelf: 'center',
  },
  bg: {
    width: 155,
    height: 200,
    resizeMode: 'contain',
    marginHorizontal: 5,
    marginTop: 10,
    marginBottom: 20,
  },
  dotsource: {
    width: 11,
    height: 11,
    resizeMode: 'contain',
    marginHorizontal: 5,
  },
  touchbox: {
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'center',
    marginTop: 'auto',
    marginBottom: 20,
  },
  touchboxText: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 25,
    fontWeight: 'bold',
    color: '#FFFFFF',
    alignSelf: 'center',
    marginTop: 60,
    textAlign: 'center',
  },
  touchboxText1: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 12,
    fontWeight: 'bold',
    color: '#FFFFFF',
    // marginTop: 10,
    alignSelf: 'center',
  },
  middleView: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginHorizontal: 15,
    marginTop: 20,
  },
});
