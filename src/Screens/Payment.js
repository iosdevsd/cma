import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,Dimensions,
  TouchableOpacity,
  Image,
  ScrollView,
} from 'react-native';
import {StatusBarLight} from '../Custom/CustomStatusBar';
import {Header, MainView} from '../Custom/CustomView';

const Payment = ({navigation}) => {
  return (
    <MainView>
      <StatusBarLight />
      <ScrollView>
        <Header onPress={navigation.goBack} title={'Payment'} />
        <View style={{height:.6,width:'100%',alignSelf:'center',marginTop:5,backgroundColor:'#FFFFFF40'}}></View>

        <View style={{width:'90%',alignSelf:'center',marginTop:20,alignItems:'center',flexDirection:'row',justifyContent:'space-between'}}>

<Text style={{fontSize:15,lineHeight:19,fontFamily:'Avenir-Bold',color:'#FFF',fontWeight:'700'}}>Amount Payable</Text>
<Text style={{fontSize:15,lineHeight:19,fontFamily:'Avenir-Bold',color:'#FFF',fontWeight:'700'}}>₹699.00</Text>
</View>
      
<View style={{height:.6,width:'100%',alignSelf:'center',marginTop:15,backgroundColor:'#FFFFFF40'}}></View>

<Text style={{fontSize:16,lineHeight:20,marginLeft:20,marginTop:20,fontFamily:'Avenir-Heavy',color:'#FFF',fontWeight:'900'}}>Payment Options</Text>




<View style={{width:'90%',alignSelf:'center',marginTop:20,alignItems:'center',flexDirection:'row',justifyContent:'space-between'}}>
<View style={{flexDirection:'row',width:'75%',alignItems:'center'}}>
<TouchableOpacity>
<Image style={{height:24,width:24,resizeMode:'contain'}} 
source={require('../assets/uncircle.png')}

/>
</TouchableOpacity>
<Text style={{fontSize:14,lineHeight:18,fontFamily:'Avenir-Medium',color:'#FFF',fontWeight:'500',marginLeft:10}}>Debit / Credit Card / Netbanking</Text>
</View>
<Image style={{height:23,width:25,resizeMode:'contain'}} 
source={require('../assets/pays.png')}

/>

</View>


<View style={{width:'90%',alignSelf:'center',marginTop:20,alignItems:'center',flexDirection:'row',justifyContent:'space-between'}}>
<View style={{flexDirection:'row',width:'75%',alignItems:'center'}}>
<TouchableOpacity>
<Image style={{height:24,width:24,resizeMode:'contain'}} 
source={require('../assets/uncircle.png')}

/>
</TouchableOpacity>
<Text style={{fontSize:14,lineHeight:18,fontFamily:'Avenir-Medium',color:'#FFF',fontWeight:'500',marginLeft:10}}>Wallet</Text>
</View>
<Image style={{height:23,width:25,resizeMode:'contain'}} 
source={require('../assets/wallet.png')}

/>

</View>




<View style={{width:'90%',alignSelf:'center',marginTop:20,alignItems:'center',flexDirection:'row',justifyContent:'space-between'}}>
<View style={{flexDirection:'row',width:'75%',alignItems:'center'}}>
<TouchableOpacity>
<Image style={{height:24,width:24,resizeMode:'contain'}} 
source={require('../assets/uncircle.png')}

/></TouchableOpacity>
<Text style={{fontSize:14,lineHeight:18,fontFamily:'Avenir-Medium',color:'#FFF',fontWeight:'500',marginLeft:10}}>Pay with Paytm</Text>
</View>
<Image style={{height:23,width:25,resizeMode:'contain'}} 
source={require('../assets/paytms.png')}

/>

</View>








<View style={{width:'90%',alignSelf:'center',marginTop:20,alignItems:'center',flexDirection:'row',justifyContent:'space-between'}}>
<View style={{flexDirection:'row',width:'75%',alignItems:'center'}}>
<TouchableOpacity>
<Image style={{height:24,width:24,resizeMode:'contain'}} 
source={require('../assets/uncircle.png')}

/>
</TouchableOpacity>
<Text style={{fontSize:14,lineHeight:18,fontFamily:'Avenir-Medium',color:'#FFF',fontWeight:'500',marginLeft:10}}>Pay on Delivery</Text>
</View>
<Image style={{height:23,width:25,resizeMode:'contain'}} 
source={require('../assets/wallet.png')}

/>

</View>













<TouchableOpacity 
onPress={()=>navigation.navigate('SuccessOrder')}
style={{width:'90%',alignSelf:'center',marginTop:180,marginBottom:40,justifyContent:'center',backgroundColor:'#3F55F6',borderRadius:10}}>

<Text style={{alignSelf:'center',marginVertical:15,fontSize:18,lineHeight:22,fontFamily:'Avenir-Medium',color:'#FFF',fontWeight:'500'}}>Pay ₹699.00</Text>

</TouchableOpacity>

      </ScrollView>
    </MainView>
  );
};

const styles = StyleSheet.create({
  topView: {
    backgroundColor: '#3F55F6',
    paddingBottom: 15,
    padding: 10,
    marginHorizontal: 20,
    borderRadius: 10,
    marginTop: 20,
  },
  topText: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 20,
    fontWeight: 'bold',
    color: '#F7F7F7',
  },
  topimage: {
    width: 31,
    height: 31,
    resizeMode: 'contain',
  },
  topText_1: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 30,
    fontWeight: 'bold',
    color: '#F7F7F7',
    marginLeft: 10,
  },
  topView_1: {
    flexDirection: 'row',
    marginTop: 15,
    alignItems: 'center',
  },
  marginView: {
    marginHorizontal: 20,
  },
  topText_2: {
    fontFamily: 'Avenir-Medium',
    fontSize: 18,
    fontWeight: '500',
    color: '#FFFFFF',
    textDecorationLine: 'underline',
    marginTop: 20,
  },
  middleView: {
    width: '90%',
    alignSelf: 'center',
  },
  middleText: {
    fontFamily: 'Avenir-Medium',
    fontSize: 18,
    fontWeight: '500',
    color: '#FFFFFF',
    marginTop: 20,
  },
  bottomText: {
    fontFamily: 'Avenir-Medium',
    fontSize: 15,
    fontWeight: '500',
    color: '#FFFFFF',
  },

  bottomText_2: {
    fontFamily: 'Avenir-Medium',
    fontSize: 12,
    fontWeight: '500',
    color: '#FFFFFF',
    opacity: 0.8,
  },

  walletImage: {
    width: 30,
    height: 33,
    resizeMode: 'contain',
    marginHorizontal: 15,
  },
  historyview: {
    flexDirection: 'row',
    marginLeft: -10,
    alignItems: 'center',
    marginTop: 20,
  },
  amount: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 18,
    fontWeight: 'bold',
    color: '#7ED321',
    marginHorizontal: 40,
  },
  historyview1: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  line: {
    borderColor: '#FFFFFF',
    borderWidth: 0.5,
    marginTop: 20,
    opacity: 0.1,
    // marginHorizontal: 10,
  },
  amount1: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 18,
    fontWeight: 'bold',
    color: '#FF001F',
    marginHorizontal: 20,
  },
});

export default Payment;
