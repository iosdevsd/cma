import React, {useEffect, useState} from 'react';
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {useDispatch, useSelector, useStore} from 'react-redux';
import * as actions from '../redux/actions';
import {htmlParse} from '../component/CommonFunction';
import HTMLView from 'react-native-htmlview';
import {Api} from '../services/Api';
import {Header, MainView} from '../Custom/CustomView';
import {StatusBarLight} from '../Custom/CustomStatusBar';
const Information = ({navigation, route}) => {
  const {information, faq} = useSelector(store => store);
  const dispatch = useDispatch();
  const store = useStore();
  useEffect(() => {
    const {key} = route.params;
    if (key === 'faq') {
      const {faq = []} = store.getState();
      if (faq.length === 0) {
        faqHandler();
      }
    } else {
      const {
        information: {privacy = '', terms_condition = '', about_us = ''},
      } = store.getState();
      if (privacy === '' || terms_condition === '' || about_us === '') {
        infromationHandler();
      }
    }
  }, []);
  const infromationHandler = async () => {
    const response = await Api.information();
    const {
      status = false,
      data: {privacy, terms_condition, about_us},
    } = response;
    if (status) {
      dispatch(
        actions.SetInformation({
          privacy: htmlParse(privacy),
          terms_condition: htmlParse(terms_condition),
          about_us: htmlParse(about_us),
        }),
      );
    } else {
      alert('5');
    }
  };
  const faqHandler = async () => {
    const response = await Api.faqs();
    consolejson('response');
    const {status = false, data = []} = response;
    if (status) {
      consolejson({data});
      dispatch(actions.SetFaq(data));
    } else {
      alert('66');
    }
  };
  return (
    <MainView>
      <StatusBarLight />
      <Header onPress={navigation.goBack} title={route.params.title} />
      <ScrollView contentContainerStyle={{flexGrow: 1, paddingVertical: 20}}>
        {route.params.key === 'faq' ? (
          faq.map(item => <FaqView item={item} />)
        ) : (
          <HTMLView
            value={information?.[route.params.key]}
            stylesheet={htmlStyles}
          />
        )}
      </ScrollView>
    </MainView>
  );
};

const FaqView = ({item: {question, answer}}) => {
  const [state, setState] = useState(false);
  return (
    <View style={styles.viewFaq}>
      <TouchableOpacity
        onPress={() => setState(!state)}
        style={styles.touchFaq}>
        <Text style={styles.textQuestionFaq}>{question}</Text>
        {state && (
          <Text Text style={styles.textAnswerFaq}>
            {answer}
          </Text>
        )}
      </TouchableOpacity>
    </View>
  );
};

const htmlStyles = StyleSheet.create({
  p: {
    fontWeight: '300',
    color: '#ffffff80', // make links coloured pink
    paddingHorizontal: 20,
  },
});
const styles = StyleSheet.create({
  viewFaq: {
    backgroundColor: 'white',
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderBottomColor: '#C8C8D3',
    borderBottomWidth: 1,
  },
  textQuestionFaq: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 14,
    color: '#83878E',
  },
  textAnswerFaq: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 14,
    color: '#9F9F9F',
  },
});
export default Information;
