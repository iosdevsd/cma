import React, {memo} from 'react';
import {Image, StyleSheet, TouchableOpacity, View, Text} from 'react-native';
import {useSelector} from 'react-redux';
const Header = memo(() => {
  const {userDetail} = useSelector(store => store);
  return (
    <View style={styles.container}>
      <Image
        source={require('../../assets/sage2.png')}
        style={styles.imageIcon}
      />
      <TouchableOpacity style={styles.touchSearch}>
        <Image
          style={styles.headerimage}
          source={require('../../assets/magnifying-glass.png')}
        />
      </TouchableOpacity>
      <TouchableOpacity style={styles.touchCoin}>
        <Image
          style={styles.imageCoin}
          source={require('../../assets/coin.png')}
        />
        <Text style={styles.textCoin}>{userDetail?.wallet}</Text>
      </TouchableOpacity>
    </View>
  );
});
const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    paddingHorizontal: 20,
    paddingVertical: 10,
    alignItems: 'center',
  },
  imageIcon: {
    width: 40,
    height: 40,
    resizeMode: 'contain',
  },
  touchSearch: {
    marginLeft: 'auto',
    marginRight: 10,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: '#FFFFFF33',
    paddingHorizontal: 12,
    paddingVertical: 4,
  },
  headerimage: {
    height: 20,
    width: 20,
    resizeMode: 'contain',
    alignSelf: 'center',
  },
  touchCoin: {
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 25,
    borderWidth: 1,
    borderColor: '#FFFFFF33',
    paddingHorizontal: 12,
    paddingVertical: 4,
  },
  imageCoin: {
    width: 25,
    height: 25,
    marginRight: 5,
  },
  textCoin: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 18,
    fontWeight: 'bold',
    color: '#FFFFFF',
  },
});

export default Header;
