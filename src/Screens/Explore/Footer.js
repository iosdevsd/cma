import React, {memo} from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {useNavigation} from '@react-navigation/core';
const Footer = memo(() => {
  const navigation = useNavigation();
  return (
    <View style={footerStyle.container}>
      <Text style={footerStyle.textTitle}>Quick Links</Text>
      <View style={{paddingVertical: 10}}>
        <TouchableOpacity style={footerStyle.touch}>
          <Text style={footerStyle.textSubTitle}>Help & Support</Text>
          <Image
            source={require('../../assets/right-arrow.png')}
            style={footerStyle.imageArrow}
          />
        </TouchableOpacity>
        <View style={footerStyle.hzLine} />
        <TouchableOpacity
          style={footerStyle.touch}
          onPress={() => {
            navigation.navigate('Information', {
              title: 'Term & Condition',
              key: 'terms_condition',
            });
          }}>
          <Text style={footerStyle.textSubTitle}>Terms & Conditions</Text>
          <Image
            source={require('../../assets/right-arrow.png')}
            style={footerStyle.imageArrow}
          />
        </TouchableOpacity>
      </View>
      <Text style={footerStyle.textSubTitle2}>Be spirited {`\n`}Fearless</Text>
      <Text style={footerStyle.textSubTitle3}>© CALLMYASTRO</Text>
    </View>
  );
});
const footerStyle = StyleSheet.create({
  container: {
    padding: 20,
  },
  textTitle: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    color: '#FFFFFF',
  },
  touch: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  hzLine: {
    borderColor: '#FFFFFF33',
    borderWidth: 0.5,
    marginVertical: 10,
  },
  textSubTitle: {
    fontFamily: 'Avenir-Medium',
    fontSize: 13,
    fontWeight: '500',
    color: '#FFFFFF',
  },
  imageArrow: {
    width: 4,
    height: 7,
    resizeMode: 'contain',
  },
  textSubTitle2: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 30,
    fontWeight: 'bold',
    color: '#FFFFFF',
  },
  textSubTitle3: {
    fontFamily: 'Avenir-Medium',
    fontSize: 15,
    fontWeight: '500',
    color: '#FFFFFFCC',
    marginTop: 10,
  },
});
export default Footer;
