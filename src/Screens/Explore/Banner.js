import React, {memo, useEffect, useState} from 'react';
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  Dimensions,
  TouchableOpacity,
  View,
} from 'react-native';
import Heading from './Heading';
import {Api} from './../../services/Api';

import {useNavigation} from '@react-navigation/native';
import {typeAlias} from '@babel/types';
const Banner = memo(() => {
  const navigation = useNavigation();
  const [horoscope, sethoroscope] = useState([]);

  useEffect(async () => {
    const response = await Api.home1({id: '1'});
    const {status = false, banners = []} = response;
    //salert(JSON.stringify(horoscopes));
    sethoroscope(banners);
  }, []);

  return (
    <View>
      <ScrollView contentContainerStyle={styles.scrollContainer} horizontal>
        {horoscope.map(item => {
          return (
            <View>
              <Image source={{uri: item.imageUrl}} style={styles.image} />
            </View>
          );
        })}
      </ScrollView>
    </View>
  );
});

export default Banner;

const styles = StyleSheet.create({
  scrollContainer: {
    paddingHorizontal: 5,
  },
  touch: {
    alignItems: 'center',
    marginHorizontal: 5,
    height: 80,
    width: 80,
  },
  touch1: {
    alignItems: 'center',
    marginHorizontal: 5,
    borderWidth: 1,
    borderColor: 'white',
    borderRadius: 30,
    height: 60,
    width: 60,
  },
  image: {
    width: Dimensions.get('window').width - 80,
    height: 150,
    alignSelf: 'center',
    marginTop: 12,
    margin: 10,
    borderRadius: 12,
  },
  textName: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    fontSize: 14,
    color: '#FFFFFF',
  },
});
