import React, {memo, useEffect, useState} from 'react';
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Heading from './Heading';
import {Api} from './../../services/Api';

import {useNavigation} from '@react-navigation/native';
import {typeAlias} from '@babel/types';
const Horoscope = memo(() => {
  const navigation = useNavigation();
  const [horoscope, sethoroscope] = useState([]);

  useEffect(async () => {
    const response = await Api.home1({id: '1'});
    const {status = false, horoscopes = []} = response;
    //salert(JSON.stringify(horoscopes));
    sethoroscope(horoscopes);
  }, []);

  return (
    <View>
      <Heading title={'Daily Horoscope'} flag={false} onPress={() => {}} />
      <ScrollView contentContainerStyle={styles.scrollContainer} horizontal>
        {horoscope.map(item => {
          return (
            <TouchableOpacity
              style={styles.touch}
              onPress={() => navigation.navigate('HoroscopeDetail', item)}>
              <View style={styles.touch1}>
                <Image source={{uri: item.imageUrl}} style={styles.image} />
              </View>
              <Text style={styles.textName}>{item.name}</Text>
            </TouchableOpacity>
          );
        })}
      </ScrollView>
    </View>
  );
});

export default Horoscope;

const styles = StyleSheet.create({
  scrollContainer: {
    paddingHorizontal: 5,
  },
  touch: {
    alignItems: 'center',
    marginHorizontal: 5,
    height: 80,
    width: 80,
  },
  touch1: {
    alignItems: 'center',
    marginHorizontal: 5,
    borderWidth: 1,
    borderColor: 'white',
    borderRadius: 30,
    height: 60,
    width: 60,
  },
  image: {
    width: 40,
    height: 40,
    alignSelf: 'center',
    resizeMode: 'contain',
    marginTop: 12,
  },
  textName: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    fontSize: 14,
    color: '#FFFFFF',
  },
});
