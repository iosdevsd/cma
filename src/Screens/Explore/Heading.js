import React, {memo} from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';

const Heading = memo(({title, flag, onPress}) => {
  return (
    <View style={styles.container}>
      <Image
        source={require('../../assets/Group.png')}
        style={styles.imageIcon}
      />
      <Text style={styles.textTitle}>{title}</Text>
      {flag && (
        <TouchableOpacity onPress={onPress} style={styles.touchSeeAll}>
          <Text style={styles.textSeeAll}>See all</Text>
          <Image
            source={require('../../assets/right-arrow.png')}
            style={styles.imageArrow}
          />
        </TouchableOpacity>
      )}
    </View>
  );
});
const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    paddingHorizontal: 20,
    paddingVertical: 10,
    alignItems: 'center',
  },
  imageIcon: {
    width: 15,
    height: 15,
  },
  textTitle: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 18,
    fontWeight: 'bold',
    color: '#FFFFFF',
    marginHorizontal: 10,
  },
  touchSeeAll: {
    marginLeft: 'auto',
    flexDirection: 'row',
    alignItems: 'center',
    padding: 5,
  },
  textSeeAll: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 12,
    fontWeight: '500',
    color: '#677294',
    marginHorizontal: 5,
  },
  imageArrow: {
    width: 4,
    height: 7,
    resizeMode: 'contain',
  },
});

export default Heading;
