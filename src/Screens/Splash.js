import React, {useEffect} from 'react';
import {
  View,
  Text,
  Image,
  Dimensions,
  StyleSheet,
  PermissionsAndroid,
} from 'react-native';
import {useStore} from 'react-redux';
import {useDispatch} from 'react-redux';
import {StatusBarLight} from '../Custom/CustomStatusBar';
import {MainView} from '../Custom/CustomView';
import {Api, LocalStorage} from '../services/Api';
import {_RemoveAuthToken, _SetAuthToken} from '../services/ApiSauce';
import * as actions from '../redux/actions';
const {height} = Dimensions.get('window');

const Splash = ({navigation}) => {
  const dispatch = useDispatch();
  const store = useStore();
  const screenHandler = async () => {
    setTimeout(() => {
      navigation.replace('SelectOption');
    }, 900);
  };

  const permissionHandler = async () => {
    const cameraPermission = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.CAMERA,
      {
        title: 'App Camera Permission',
        message: 'App needs access to your camera ',
        buttonNeutral: 'Ask Me Later',
        buttonNegative: 'Cancel',
        buttonPositive: 'OK',
      },
    );
    const audioPermission = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
      {
        title: 'App Audio Permission',
        message: 'App needs access to microphone ',
        buttonNeutral: 'Ask Me Later',
        buttonNegative: 'Cancel',
        buttonPositive: 'OK',
      },
    );

    if (
      cameraPermission === PermissionsAndroid.RESULTS.GRANTED &&
      audioPermission === PermissionsAndroid.RESULTS.GRANTED
    ) {
      navigationHandler();
      console.log('Camera permission given');
    } else {
      console.log('Camera permission denied');
    }
  };
  const navigationHandler = async () => {
    // let isFirstTimeOpen =
    //   ((await LocalStorage.getFirstTimeOpen()) || 'true') === 'true';

    const token = (await LocalStorage.getToken()) || '';
    consolejson({token});
    if (token.length !== 0) {
      _SetAuthToken(token);
      const {deviceInfo} = store.getState();
      const response = await Api.userProfile({
        device_id: deviceInfo.id,
        device_type: deviceInfo.os,
        device_token: deviceInfo.token,
        model_name: deviceInfo.name,
      });
      const {status = false, data = {}} = response;
      if (status) {
        dispatch(actions.SetUserDetail(data));
        navigation.replace('TabNavigator');
      } else {
        _RemoveAuthToken();
        LocalStorage.setToken('');
        navigation.replace('Login');
      }
    } else {
      setTimeout(() => {
        navigation.replace('Login');
      }, 1500);
    }
  };
  useEffect(() => {
    screenHandler();
    permissionHandler();
  }, []);

  return (
    <MainView>
      <StatusBarLight />
      <Image source={require('../assets/logo.png')} style={styles.logo} />
    </MainView>
  );
};

const styles = StyleSheet.create({
  logo: {
    marginTop: height / 3,
    width: 196,
    height: 196,
    resizeMode: 'contain',
    alignSelf: 'center',
  },
});

export default Splash;
