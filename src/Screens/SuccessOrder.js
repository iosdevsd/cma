import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  Dimensions,
  TouchableOpacity,
  Image,
  ScrollView,
  ImageBackground,
} from 'react-native';
import {StatusBarLight} from '../Custom/CustomStatusBar';
import {Header, MainView} from '../Custom/CustomView';

const SuccessOrder = ({navigation}) => {
  return (
    <MainView>
      <ImageBackground
        style={{height: '100%', width: '100%'}}
        source={require('../assets/bgimg.png')}>
        <View style={{width: '90%', alignSelf: 'center', marginTop: 70}}>
          <Image
            style={{height: 80, width: 80, resizeMode: 'contain'}}
            source={require('../assets/checked.png')}
          />

          <Text style={styles.topText_1}>Success!</Text>

          <Text
            style={{
              marginTop: 7,
              fontSize: 14,
              lineHeight: 18,
              fontFamily: 'Avenir-Medium',
              color: '#FFF',
              fontWeight: '500',
            }}>
            Order ID 12345 | 2021-02-07 | 07:40:00
          </Text>
          <Text
            style={{
              fontSize: 20,
              marginTop: 24,
              lineHeight: 30,
              fontFamily: 'Avenir-Heavy',
              color: '#FFF',
              fontWeight: '900',
            }}>
            Your payment has been,processed.
          </Text>

          <Text style={styles.topText_1}>₹699.00</Text>

          <View
            style={{
              width: '100%',
              alignSelf: 'center',
              marginTop: 20,
              alignItems: 'center',
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>
            <TouchableOpacity
              onPress={() => navigation.navigate('OrderDetail')}>
              <Text
                style={{
                  fontSize: 16,
                  lineHeight: 20,
                  fontFamily: 'Avenir-Medium',
                  color: '#FFF',
                  fontWeight: '500',
                }}>
                Order Details
              </Text>
            </TouchableOpacity>

            <Image
              style={{height: 12, width: 7, resizeMode: 'contain'}}
              source={require('../assets/right-arrow.png')}
            />
          </View>

          <View
            style={{
              height: 0.6,
              width: '100%',
              alignSelf: 'center',
              marginVertical: 15,
              backgroundColor: '#FFFFFF40',
            }}></View>

          <View
            style={{
              width: '100%',
              alignSelf: 'center',
              marginTop: 20,
              alignItems: 'center',
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>
            <TouchableOpacity>
              <Text
                style={{
                  fontSize: 16,
                  lineHeight: 20,
                  fontFamily: 'Avenir-Medium',
                  color: '#FFF',
                  fontWeight: '500',
                }}>
                Shipping Address
              </Text>
            </TouchableOpacity>

            <Image
              style={{height: 12, width: 7, resizeMode: 'contain'}}
              source={require('../assets/right-arrow.png')}
            />
          </View>

          <View
            style={{
              height: 0.6,
              width: '100%',
              alignSelf: 'center',
              marginVertical: 15,
              backgroundColor: '#FFFFFF40',
            }}></View>

          <View
            style={{
              width: '100%',
              alignSelf: 'center',
              marginTop: 20,
              alignItems: 'center',
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>
            <TouchableOpacity>
              <Text
                style={{
                  fontSize: 16,
                  lineHeight: 20,
                  fontFamily: 'Avenir-Medium',
                  color: '#FFF',
                  fontWeight: '500',
                }}>
                View Invoice
              </Text>
            </TouchableOpacity>

            <Image
              style={{height: 12, width: 7, resizeMode: 'contain'}}
              source={require('../assets/right-arrow.png')}
            />
          </View>

          <View
            style={{
              height: 0.6,
              width: '100%',
              alignSelf: 'center',
              marginVertical: 15,
              backgroundColor: '#FFFFFF40',
            }}></View>
        </View>
      </ImageBackground>
    </MainView>
  );
};

const styles = StyleSheet.create({
  topView: {
    backgroundColor: '#3F55F6',
    paddingBottom: 15,
    padding: 10,
    marginHorizontal: 20,
    borderRadius: 10,
    marginTop: 20,
  },
  topText: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 20,
    fontWeight: 'bold',
    color: '#F7F7F7',
  },
  topimage: {
    width: 31,
    height: 31,
    resizeMode: 'contain',
  },
  topText_1: {
    marginTop: 20,
    fontFamily: 'Avenir-Black',
    fontSize: 35,
    lineHeight: 39,
    fontWeight: '900',
    color: '#FFF',
  },
  topView_1: {
    flexDirection: 'row',
    marginTop: 15,
    alignItems: 'center',
  },
  marginView: {
    marginHorizontal: 20,
  },
  topText_2: {
    fontFamily: 'Avenir-Medium',
    fontSize: 18,
    fontWeight: '500',
    color: '#FFFFFF',
    textDecorationLine: 'underline',
    marginTop: 20,
  },
  middleView: {
    width: '90%',
    alignSelf: 'center',
  },
  middleText: {
    fontFamily: 'Avenir-Medium',
    fontSize: 18,
    fontWeight: '500',
    color: '#FFFFFF',
    marginTop: 20,
  },
  bottomText: {
    fontFamily: 'Avenir-Medium',
    fontSize: 15,
    fontWeight: '500',
    color: '#FFFFFF',
  },

  bottomText_2: {
    fontFamily: 'Avenir-Medium',
    fontSize: 12,
    fontWeight: '500',
    color: '#FFFFFF',
    opacity: 0.8,
  },

  walletImage: {
    width: 30,
    height: 33,
    resizeMode: 'contain',
    marginHorizontal: 15,
  },
  historyview: {
    flexDirection: 'row',
    marginLeft: -10,
    alignItems: 'center',
    marginTop: 20,
  },
  amount: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 18,
    fontWeight: 'bold',
    color: '#7ED321',
    marginHorizontal: 40,
  },
  historyview1: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  line: {
    borderColor: '#FFFFFF',
    borderWidth: 0.5,
    marginTop: 20,
    opacity: 0.1,
    // marginHorizontal: 10,
  },
  amount1: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 18,
    fontWeight: 'bold',
    color: '#FF001F',
    marginHorizontal: 20,
  },
});

export default SuccessOrder;
