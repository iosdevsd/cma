import React, {useState} from 'react';
import {
  FlatList,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {StatusBarLight} from '../Custom/CustomStatusBar';
import {Header, MainView} from '../Custom/CustomView';

const SelectDateTime = () => {
  const [people, setPeople] = useState([
    {
      key: 1,
      day: 'Today',
      date: '28',
    },
    {
      key: 2,
      day: 'Tue',
      date: '29',
    },
    {
      key: 3,
      day: 'Wed',
      date: '30',
    },
    {
      key: 4,
      day: 'Thu',
      date: '31',
    },
    {
      key: 5,
      day: 'Fri',
      date: '1',
    },
  ]);

  return (
    <MainView>
      <StatusBarLight />
      <ScrollView>
        <Header
          onPress={() => navigation.navigate('')}
          title={'Select Date & Time'}
        />

        <Text style={styles.topText}>Select Date</Text>

        <FlatList
          data={people}
          // numColumns={2}
          horizontal={true}
          renderItem={({item}) => {
            return (
              <View style={styles.ftView}>
                <Text style={styles.textDay}>{item.day}</Text>
                <Text style={styles.topText2}>{item.date}</Text>
              </View>
            );
          }}
        />

        <Text style={styles.topText}>Select Time</Text>

        <View style={styles.middleview}>
          <View style={styles.middleview1}></View>
          <View style={styles.middleview2}></View>
        </View>
      </ScrollView>
    </MainView>
  );
};

export default SelectDateTime;

const styles = StyleSheet.create({
  ftView: {
    width: 64,
    height: 84,
    marginHorizontal: 10,
    marginTop: 10,
    // marginLeft: 7,
    // marginVertical: 0,
    padding: 5,
    borderRadius: 15,
    backgroundColor: '#13042A',
    // backgroundColor: 'yellow',
  },
  topText: {
    fontFamily: 'Avenir-Medium',
    fontSize: 16,
    fontWeight: '500',
    color: '#FFFFFF',
    marginTop: 20,
    marginHorizontal: 20,
    // alignSelf: 'baseline',
  },

  topText2: {
    fontFamily: 'Avenir-Medium',
    fontSize: 26,
    fontWeight: '500',
    color: '#FFFFFF',
    marginTop: 10,
    alignSelf: 'center',
    // marginHorizontal: 20,
  },
  textDay: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 13,
    color: '#F5F8FA',
    marginTop: 5,
    // marginVertical: 10,
    alignSelf: 'center',
  },
  middleview: {
    flexDirection: 'row',
    alignSelf: 'center',
    justifyContent: 'space-between',
    marginHorizontal: 20,
    // backgroundColor: 'yellow',
  },
  middleview1: {
    width: '30%',
    padding: 10,
    backgroundColor: '#3F55F6',
  },
  middleview2: {
    width: '30%',
    padding: 10,
    backgroundColor: '#13042A',
  },
});
