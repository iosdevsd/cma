import React from 'react';
import {
  Image,
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {FilledTextField, TextField} from 'react-native-material-textfield';

export const MainView = props => (
  <SafeAreaView style={{backgroundColor: '#0E0220', flex: 1}} {...props} />
);

export const ButtonStyle = (
  title,
  bgColor = '#FFFFFF',
  txtcolor = '#FFFFFF',
  onPress,
) => (
  <TouchableOpacity
    activeOpacity={0.6}
    onPress={onPress}
    style={[styles.facebookButton, {backgroundColor: bgColor}]}>
    <Text style={[styles.facebooktext, {color: txtcolor}]}>{title}</Text>
  </TouchableOpacity>
);

export const CustomInputField = props => (
  <FilledTextField
    label="Name"
    fontSize={18}
    textColor={'#FFFFFF'}
    tintColor={'#FFFFFF'}
    // labelTextStyle={{color: 'yellow'}}
    baseColor="#FFFFFF"
    labelFontSize={16}
    containerStyle={styles.containerStyle}
    inputContainerStyle={styles.inputContainerStyle}
    lineWidth={0}
    activeLineWidth={0}
    {...props}
  />
);
export const TextError = ({title}) =>
  title.lenght !== 0 ? (
    <View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignSelf: 'center',
      }}>
      <Text style={styles.textError}>{title}</Text>
    </View>
  ) : null;

const styles = StyleSheet.create({
  facebookButton: {
    backgroundColor: '#3B5998',
    padding: 15,
    marginHorizontal: 25,
    borderRadius: 10,
    marginTop: 20,
  },
  facebooktext: {
    fontFamily: 'Avenir-Medium',
    fontSize: 14,
    fontWeight: '600',
    color: '#FFFFFF',
    alignSelf: 'center',
  },
  containerStyle: {
    width: '90%',
    // height: '10%',
    borderRadius: 10,
    alignSelf: 'center',
    backgroundColor: '#1B172C',
    marginTop: 40,
  },
  inputContainerStyle: {
    marginHorizontal: 20,
    backgroundColor: '#1B172C',
  },

  textError: {
    fontFamily: 'Avenir-Medium',
    fontSize: 16,
    fontWeight: '500',
    color: '#100C08',
  },
});

export const Header = props => (
  <View style={headerStyle.viewHeader}>
    <TouchableOpacity style={headerStyle.touchBack} onPress={props.onPress}>
      <Image
        source={require('../assets/back.png')}
        style={headerStyle.imageBack}
      />
    </TouchableOpacity>
    <Text style={headerStyle.textTitle}>{props.title}</Text>
  </View>
);

const headerStyle = StyleSheet.create({
  viewHeader: {
    backgroundColor: '#0E0220',
    flexDirection: 'row',
    paddingTop: 20 + StatusBar.currentHeight,
    paddingHorizontal: 20,
    paddingBottom: 20,
    alignItems: 'center',
  },
  imageBack: {
    width: 12,
    height: 22,
    resizeMode: 'contain',
  },
  touchBack: {
    padding: 5,
  },
  textTitle: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 20,
    color: '#FFFFFF',
    marginHorizontal: 20,
  },
  flexView: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});

export const BottomButton = props => (
  <TouchableOpacity
    activeOpacity={0.8}
    style={bottomStyle.bottomView}
    onPress={props.onPress}>
    <Text style={bottomStyle.textTitle}>{props.bottomtitle}</Text>
  </TouchableOpacity>
);

export const bottomStyle = StyleSheet.create({
  bottomView: {
    width: '90%',
    alignSelf: 'center',
    backgroundColor: '#3F55F6',
    borderRadius: 5,
    padding: 10,
    marginTop: 50,
    marginBottom: 10,
  },
  textTitle: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 18,
    color: '#FFFFFF',
    textAlign: 'center',
  },
});
